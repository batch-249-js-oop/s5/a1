'use strict';

class Product {
    constructor(name,price){
        if(typeof name === 'string' && typeof price === 'number'){
            this.name = name;
            this.price = price;
        }
        this.isActive = true;
    }
    //methods
    archive(){
        if(this.isActive){
            this.isActive = false;
        } else {
            this.isActive = true;
        }
        return this;
    };
    updatePrice(updatedPrice){
        if(typeof updatedPrice === 'number'){
            this.price = updatedPrice;
        }
        return this;
    }
    
}

class Cart{
    constructor(){
        this.contents = [];
        this.totalAmount = 0;
    }
    //methods
    addToCart(product,quantity){
        if(product.constructor === Product && typeof quantity === 'number'){
            if(product.isActive){
                let addedProduct = {
                    product: product,
                    quantity:quantity,
                };
                this.contents.push(addedProduct);
            }
        }
        return this;
    }
    showCartContents(){
        //logs individual cart content instead of a simple array
        this.contents.forEach(content => {
            let cartContent = {
                name: content.product.name,
                price: content.product.price,
                quantity: content.quantity,
            }
            console.log(cartContent);
        })
        //console.log(this.contents);
        return this;
    }
    updateProductQuantity(productName, updatedProductQuantity){
        if(typeof productName === 'string' && typeof updatedProductQuantity === 'number'){
            this.contents.find(content => {
                return content.product.name === productName
            }).quantity = updatedProductQuantity;
        }
        return this;
    }
    clearCartContents(){
        this.contents = [];
        this.totalAmount = 0;
        return this;
    }
    computeTotal(){
        this.contents.forEach(content => {
            this.totalAmount += (content.quantity*content.product.price);
        })
        return this;
    }
}

class Customer{
    constructor(email){
        if(typeof email === 'string'){
            this.email = email;
        }
        this.orders = [];
        this.cart = new Cart();
    }
    //methods
    checkOut(){
        if(this.cart.contents.length !==0){
            this.cart.contents.forEach(content => {
                let checkOutProduct = {
                    name: content.product.name,
                    price: content.product.price,
                    quantity: content.quantity,
                    subtotalAmount: content.product.price*content.quantity,
                }
                this.cart.totalAmount += content.quantity*content.product.price;
                this.orders.push(checkOutProduct);
            })
        }
        //alternate solution is to call computeTotal method within the checkout method of our Customer class before returning `this`
        //this.cart.computeTotal();
        return this;
    }
}

//customer creation:
const john = new Customer ('john@mail.com');
//product creation:
const prodA = new Product ('soap', 9.99);
const prodB = new Product ('sponge', 15.75);


//product update price:
console.log('productA', prodA);
prodA.updatePrice(12.99)
console.log('updated productA price:', prodA);
//archive product
prodA.archive();
console.log('productA archive', prodA);

//add to cart
prodA.archive();
john.cart.addToCart(prodA, 3);
john.cart.addToCart(prodB, 2);

//show cart
console.log(`---show cart---`);
john.cart.showCartContents();

//update product quantity in cart
console.log(`-----update product quantity in cart-----`)
john.cart.updateProductQuantity('soap',5);
john.cart.showCartContents();

//clear cart contents
// console.log(`---clear cart contents---`);
// john.cart.clearCartContents();
// console.log(john)

//compute cart total
console.log(`--compute cart total--`);
john.cart.computeTotal();
console.log(john.cart);

//checkout
console.log(`----checkout----`);
john.checkOut();
console.log(john.orders);

//happy weekend!